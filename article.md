title: "Proč české studenty láká víc Španělsko než Polsko"
perex: "Server iROZHLAS.cz analyzoval data o desítkách tisíc českých studentů, kteří vyjeli do zahraničí s výměnným programem Erasmus."
authors: ["Petr Kočí"]
published: "29. června 2017"
coverimg: https://interaktivni.rozhlas.cz/data/erasmus-snwfl/media/cover.jpg
coverimg_note: "Ilustrační foto z filmu Erasmus a spol."
styles: []
libraries: ["https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js", "https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js", "https://cdnjs.cloudflare.com/ajax/libs/jquery.isotope/3.0.4/isotope.pkgd.min.js", "https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.17/d3.min.js", "https://cdnjs.cloudflare.com/ajax/libs/queue-async/1.0.7/queue.min.js"]
options: "" #wide
---

Zatímco do Španělska zamířilo v uplynulém akademickém roce na studijní pobyty a stáže osm stovek českých vysokoškoláků, podobně lidnaté Polsko jich přilákalo čtvrtinu. Vyplývá to z dat o desítkách tisíc absolventů výměnného programu Evropské unie Erasmus. Ten letos v červnu [slaví](https://ec.europa.eu/programmes/erasmus-plus/anniversary/30th-anniversary-and-you_cs) 30 let od svého vzniku. České školy se ho účastní od roku 1998, od té doby díky němu vycestovalo do zahraničí přes čtvrt milionu českých studentů i pedagogů.

"Ve Španělsku je pro české studenty velkým lákadlem španělština, druhý nejrozšířenější jazyk na světě. Roli hraje také široká nabídka univerzit a někdy samozřejmě i klima a moře," vysvětluje Lucie Durcová z Domu zahraniční spolupráce ministerstva školství.

Naproti tomu Polsko, podobně jako Slovensko a Maďarsko podle ní mnohé studenty odrazuje svou kulturní – a v případě prvních dvou zemí i jazykovou - blízkostí. "Když už tam někdo jede, tak hlavně do Budapešti," popisuje propagátorka Erasmu svou zkušenost, kterou získala jako vedoucí tohoto programu na Masarykově univerzitě v Brně. V opačném směru je bariéra menší: Slováci jsou třetí a Poláci pátí na žebříčku studentů, kteří přijíždějí do Česka.  

## Kam jezdí čeští vysokoškoláci na Erasmus?

Data o všech českých vysokoškolácích, kteří s Erasmem vycestovali od roku 2005, si můžete prostudovat v interaktviním grafu. Údaje za akademický rok 2016/2017 nejsou definitivní, čísla představují odhad DZS na základě posledních dostupných čísel.

<wide>
  <div class="container">
    <div id="lead">
    </div>
    <div id="main">
      <div id="button-wrap">
        <div id="count" class="button active">DLE POČTU</div>
        <div id="name" class="button">ABECEDNĚ</div>
      </div>
      
<div id="vis"></div>
</div>
</div> <script>
    $(document).ready(function() {
    });
  </script>
Zdroj dat: [Dům zahraniční spolupráce](http://www.dzs.cz/), vizualizace využívá [kód Jima Vallandinghama](https://github.com/vlandham/small_mults) 
</wide>

Dlouhodobě nejpopulárnější destinace českých "erasmáků" je Německo. Roli při jeho výběru hraje dobrá dopravní dostupnost, pestrá nabídka univerzit i možnost navázat kontakty a ucházet se pak případně v hospodářsky nejvýkonnější evropské zemi o zaměstnání.

Studenti nemají při výběru země pro studijní pobyt úplně volnou ruku, záleží na jejich škole či fakultě, jaké smlouvy si vyjedná se zahraničními univerzitami. Bez pomoci školy si mohou studenti sami domluvit stáže v konkrétních podnicích či ve školách, pak nejsou svázáni aktuální nabídkou míst a mohou jet do libovolné země EU. Takových stáží jsou ale každý rok jen stovky, na studijní pobyty vyjíždí ročně přes osm tisíc českých vysokoškoláků.

## Opadá zájem o Řecko a Turecko

Do programu Erasmus je zapojené i Turecko, poptávka po studiu v Istanbulu či Ankaře však od roku 2014 ochabla. Místo tří stovek před dvěma lety zamířilo letos za Bospor méně než osmdesát studentů. "Může za to jednoznačně nestabilní politická situace, obavy o bezpečnost a teroristické útoky," vysvětluje Durcová z Domu zahraniční spolupráce.

Podobně opadl zájem o Řecko. "Studenti přijeli a často zjistili, že univerzity vyhlásily generální stávku a prostě se neučilo, tudíž nebylo co a kde studovat." Podle podmínek Erasmu přitom musejí jeho účastníci splnit studijní povinnosti a získat za ně určitý počet kreditů, jinak jim hrozí, že budou muset vrátit stipendium, v krajním případě i vyloučení ze studia na domovské škole. "To bude důvod, proč se Řecku vyhýbají," domnívá se Lucie Durcová.

Účastníci Erasmu dostávají od Evropské unie paušální stipendium, částka se liší podle toho, kam jedou. V nejdražších, například skandinávských zemích, pobírají 500 euro (13 tisíc korun) za měsíc, ve středně drahých 400 euro (10 tisíc korun) a v nejlevnějších 300 euro (8 tisíc) měsíčně. Pokud jedou na individuálně domluvenou stáž, dostanou studenti měsíčně v každé kategorii ještě o 100 euro víc. Se stipendiem mohou nakládat podle svého uvážení. Výhodou Erasmu je, že jeho účastníci neplatí školné ani v zemích, kde se běžně školné platí.

Evropská unie na cesty českých vysokoškoláků v programu Erasmus přispěje příští rok 22 miliony eur (580 milionů korun), zhruba stejnou částku zaplatí ministerstvo školství jako příspěvek vysokým školám na internacionalizaci. 